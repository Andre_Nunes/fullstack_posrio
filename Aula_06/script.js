var login = document.getElementById("login");
var senha = document.getElementById("senha");
var btnEntrar = document.getElementById("entrar");
var edtSenha = document.getElementById("senha");
var formLogin = document.getElementById("entrar");
btnEntrar.addEventListener("click", validarUsuarioESenha());
edtSenha.addEventListener("keyup", e=>{this.entrar(e)});

function entrar(value){
    if (value.key == "Enter") {
        return; 
    }else{
        validarUsuarioESenha()
    }    
}

function validarUsuarioESenha(){  
    if (usuarioESenhaEstaoCorrertos(login, senha)){
        abrirPagina("cadasatroUsuarios.html");
    }else{
        showAlertAcessoNegado();
    }       
}

function usuarioESenhaEstaoCorrertos(pLogin, pSenha){
    if ((pLogin.value.toUpperCase() == "ADMIN") && (pSenha.value == 123)){
        return true;
    }
    return false;
}

function showAlertAcessoNegado(){  
   toastr.options = {
    "closeButton": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "timeOut": "5000",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
  }
  toastr.error("Acesso negado");
}

function abrirPagina(pPagina){
    window.open(pPagina);
}

