var btnCadastrar = document.querySelector('#btn-cadastrar');
var nome = document.querySelector('#ipt-nome');
var idade = document.querySelector('#ipt-idade');
var endereco = document.querySelector('#ipt-endereco');
var bairro = document.querySelector('#ipt-bairro');
var telefone = document.querySelector('#ipt-telefone');
var tabela = document.querySelector('#tbl-pessoa tbody');
var divCadastro = document.querySelector('#div-cadastro');

btnCadastrar.addEventListener('click', e=>{
    if (cadastroNaoConcluido()){
        return;
    }
    cadastrarPessoa();
    limparCampos();
})

var  pessoa = {
    nome: nome.value,
    idade: idade.value
}
function cadastroNaoConcluido(){
    if (nome.value == ''){
        alertaCadastroNaoConcluido('nome');
        return true;    
    }
    if (endereco.value == ''){
        alertaCadastroNaoConcluido('endereco'); 
        return true ;   
    }
    if (bairro.value == ''){
        alertaCadastroNaoConcluido('bairro'); 
        return true;   
    }
    if (telefone.value == ''){
        alertaCadastroNaoConcluido('telefone');
        return true;    
    }            
};

function alertaCadastroNaoConcluido(pCampo){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
    }
    toastr.error("O campo " + pCampo + " é obrigatório");
}

function cadastrarPessoa(){
    var linha = document.createElement('tr');
    var coluna = document.createElement('td');

    coluna.textContent = nome.value;
    linha.appendChild(coluna);

    coluna = document.createElement('td');
    coluna.textContent = idade.value;
    linha.appendChild(coluna);

    coluna = document.createElement('td');
    coluna.textContent = endereco.value;
    linha.appendChild(coluna);

    coluna = document.createElement('td');
    coluna.textContent = bairro.value;
    linha.appendChild(coluna);

    coluna = document.createElement('td');
    coluna.textContent = telefone.value;
    linha.appendChild(coluna);

    tabela.appendChild(linha);
}

function limparCampos(){
    nome.value = '';
    endereco.value = '';
    bairro.value = '';
    telefone.value = '';  
    idade.value = 0;
    nome.focus();
};

$('#ipt-telefone', '#div-telefone')

.keydown(function (e) {
    var key = e.which || e.charCode || e.keyCode || 0;
    $phone = $(this);

    if ($phone.val().length === 1 && (key === 8 || key === 46)) {
        $phone.val('('); 
        return false;
    } 
    if ($phone.val().length === 15 && (key != 8 && key != 9)){
        return false; 
    }

    else if ($phone.val().charAt(0) !== '(') {
        $phone.val('('+$phone.val()); 
    }

    if (key !== 8 && key !== 9) {
        if ($phone.val().length === 3) {
            $phone.val($phone.val() + ') ');
        }
        if ($phone.val().length === 10) {
            $phone.val($phone.val() + '-');
        }
    }

    return (key == 8 || 
            key == 9 ||
            key == 46 ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105)); 
})

.bind('focus click', function () {
    $phone = $(this);

    if ($phone.val().length === 0) {
        $phone.val('(');
    }
    else {
        var val = $phone.val();
        $phone.val('').val(val); 
    }
})
