const express = require("express");
const app = express();

app.get("/", function(req, res){
    res.sendFile(__dirname + "/view/index.html")
})

app.get("/style", function(req, res){
    res.sendFile(__dirname + "/style/style.css")
})

app.get("/controller", function(req, res){
    res.sendFile(__dirname + "/controller/script.js")
})

app.get("/image", function(req, res){
    res.sendFile(__dirname + "/image/user_male_icon.svg")
})

app.listen('8090');
